from django.urls import path

from . import views

from django.conf.urls import url

urlpatterns = [
    url(r'display_feedback/(?P<session_id>[0-9]+)', views.display_feedback, name='display_feedback'),
    url(r'overview', views.overview, name='overview'),
    url(r'contributors', views.contributors, name='contributors'),
    url(r'', views.submit_feedback, name='submit_feedback')
]
