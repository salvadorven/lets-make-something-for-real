from django.db import models


class ConferenceDay(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateField()

    def __str__(self):
        return self.title

class Session(models.Model):
    title = models.CharField(max_length=100)
    conferenceDay = models.ForeignKey(ConferenceDay, related_name='sessions', blank=False, null=True, default=None, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_title_with_feedback_count(self):
        return self.title + " [" + str(self.feedbacks.all().count()) + " reviews]"

    def get_ordered_feedback_list(self):
        return self.feedbacks.all().order_by('-id')


class Feedback(models.Model):
    RATING_CHOICES = (
        (1, 'Disappointing'),
        (2, 'Mwah'),
        (3, 'Ok'),
        (4, 'Great!'),
        (5, 'Amazing!!'),
    )

    session = models.ForeignKey(Session, related_name='feedbacks', blank=False, null=False, default=None, on_delete=models.CASCADE)
    rating = models.IntegerField(choices=RATING_CHOICES)
    comment = models.CharField(max_length=1000, default='')

    def __str__(self):
        return self.session.title + " (" + str(self.rating) + ")"

class People(models.Model):
    fullname = models.CharField(max_length=100)
    presenter = models.BooleanField()
    contributor = models.BooleanField()

    def __str__(self):
        return self.fullname
