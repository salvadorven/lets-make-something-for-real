from django.contrib import admin
from feedback.models import Session, Feedback, ConferenceDay, People
# Register your models here.

admin.site.register(Session)
admin.site.register(Feedback)
admin.site.register(ConferenceDay)
admin.site.register(People)
