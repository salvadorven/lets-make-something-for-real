from django.test import TestCase
from feedback.models import Session, Feedback


class FeedbackTests(TestCase):

    def test_given_there_is_a_feedback_when_I_view_the_feedback_form_then_I_see_a_count_of_one(self):
        session = Session.objects.create(title='My first session')
        feedback = Feedback.objects.create(session=session, rating=5, comment='I loved it!')

        response = self.client.get('/submit_feedback')

        self.assertEqual(response.context['FeedbackCount'], 1)

    def test_given_a_feedback_of_3_when_I_view_the_session_details_then_I_see_an_average_of_3(self):
        session = Session.objects.create(title='My first session')
        feedback = Feedback.objects.create(session=session, rating=3, comment='I loved it!')

        response = self.client.get('/display_feedback/' + str(session.id))

        self.assertEqual(response.context['FeedbackAverage'], 3)


    def test_given_feedbacks_of_3_and_5_when_I_view_the_session_details_then_I_see_an_average_of_4(self):
        session = Session.objects.create(title='My first session')
        feedback = Feedback.objects.create(session=session, rating=3, comment='I loved it!')
        feedback = Feedback.objects.create(session=session, rating=5, comment='I loved it!')

        response = self.client.get('/display_feedback/' + str(session.id))

        self.assertEqual(response.context['FeedbackAverage'], 4)

    def test_given_feedbacks_of_different_sessions_when_I_view_the_session_details_then_I_see_the_average_of_session_only(self):
        session = Session.objects.create(title='My first session')
        feedback = Feedback.objects.create(session=session, rating=3, comment='I loved it!')
        session2 = Session.objects.create(title='My second session')
        feedback = Feedback.objects.create(session=session2, rating=1, comment='I hated it!')

        response = self.client.get('/display_feedback/' + str(session.id))

        self.assertEqual(response.context['FeedbackAverage'], 3)



