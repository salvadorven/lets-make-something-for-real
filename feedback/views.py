from django.http import HttpResponse
from django.http import HttpResponseRedirect
from annoying.decorators import render_to
from feedback.models import Session, Feedback, People
from django.contrib import messages
from django.db.models import Avg
import datetime

def index(request):
    return HttpResponse("Hello, world. Let's build something for real! More will come soon!")

@render_to('submit_feedback.html')
def submit_feedback(request):
    if request.POST:
        session = Session.objects.filter(id=request.POST['session']).first()
        if session:
            feedback = Feedback(session=session, rating=request.POST['rating'], comment=request.POST['comment'])
            feedback.save()
            return HttpResponseRedirect("/display_feedback/" + str(session.id))
            # messages.info(request, 'Thank you, your feedback has been saved

    #sessions = Session.objects.filter(conferenceDay__date=datetime.datetime.now())
    sessions = Session.objects.all()
    session_without_feedback_count = sessions.filter(feedbacks__isnull=True).count()
    return {'Sessions': sessions, 'FeedbackCount': Feedback.objects.count(), 'SessionWithoutFeedbackCount': session_without_feedback_count}

@render_to('display_feedback.html')
def display_feedback(request, session_id):
    session = Session.objects.filter(id=session_id).first()
    average = session.feedbacks.aggregate(Avg('rating'))['rating__avg']
    return { 'Session': session, 'FeedbackAverage': average }


@render_to('overview.html')
def overview(request):
    return {'feedbacks': Feedback.objects.all()}

@render_to('contributors.html')
def contributors(request):
    return {'people': People.objects.all()}
